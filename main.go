package main

import (
	"fmt"
	"os"

	"github.com/go-gota/gota/dataframe"
)

// check takes an error as input and panics if it's not nil
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	// assuming standard TAMtool output - vms tab
	csvFile := "./vms_tam_oct2019.csv"
	file, err := os.Open(csvFile)
	check(err)

	df := dataframe.ReadCSV(file)
	// TODO: converte CPU average series to a float
	//cpuAverage:= df.Col("NumCPU").Float()
	memAverage := df.Col("MemoryMB").Mean()
	diskAverage := df.Col("AllocSpace in GB").Mean()
	fmt.Println(df)
	fmt.Printf("The Average VM size is %v MB RAM, %v GB disk\n", memAverage, diskAverage)

}
